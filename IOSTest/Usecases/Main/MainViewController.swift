//
//  MainViewController.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var topicTableView: UITableView!
    @IBOutlet weak var topicView: UIView!
    @IBOutlet weak var tagView: UIView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    var viewModel = MainViewModel()
    var refreshControl = UIRefreshControl()
    
    lazy var collectionViewFlowLayout : CollectionViewFlowLayout = {
        let layout = CollectionViewFlowLayout()
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        bindViewModel()
        setupInterface()
    }
    
    func bindViewModel() {
        viewModel.fetchTopic()
        viewModel.fetchTag()
        viewModel.reloadTagCallback = {
            self.tagCollectionView.reloadData()
        }
        
        viewModel.reloadTopicCallback = {
            self.topicTableView.reloadData()
        }
    }
    
    func registerCells() {
        topicTableView.register(UINib(nibName: "TopicTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "TopicTableViewCell")
        tagCollectionView.register(UINib(nibName: "TagCollectionViewCell", bundle: nil),
                                   forCellWithReuseIdentifier: "TagCollectionViewCell")
    }
    
    func setupInterface() {
        scrollView.delegate = self
        tagCollectionView.collectionViewLayout = self.collectionViewFlowLayout
        topicView.addBorder(toSide: [.Top, .Bottom], withColor: UIColor(named: "purple-light") ?? UIColor.white, andThickness: 1.0)
        tagView.addBorder(toSide: [.Top, .Bottom], withColor: UIColor(named: "purple-light") ?? UIColor.white, andThickness: 1.0)
        
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        scrollView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewModel.fetchTopic()
        viewModel.fetchTag()
        refreshControl.endRefreshing()
    }
}

extension MainViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       let height = scrollView.frame.size.height
       let contentYoffset = scrollView.contentOffset.y
       let distanceFromBottom = scrollView.contentSize.height - contentYoffset
       if distanceFromBottom < height {
            if !viewModel.topicsTemp.isEmpty {
                viewModel.loadMoreTopic()
            }
       }
   }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.topics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        heightConstraint.constant = tableView.contentSize.height
        return viewModel.displayTopicCell(tableView: tableView, cellForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
     return 65
     }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return viewModel.displayTagCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
}
