//
//  TagCollectionViewCell.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var primaryTextLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
    }

    func setupInterface(tag: Tag) {
        primaryTextLabel.text = tag.name
        
        if let url = URL(string: tag.imageUrl?[1] ?? "") {
            coverImage.downloadImage(from: url)
        } else {
            coverImage.image = UIImage(named: "")
        }
    }
}
