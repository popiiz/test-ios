//
//  TopicTableViewCell.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coverImage.layer.cornerRadius = 2
    }

    func setupInterface(topic: Topic) {
        titleLabel.text = topic.title
        authorLabel.text = topic.author
        
        let viewCount = convertNumber(str: topic.viewCount)
        viewCountLabel.text = viewCount

        if let url = URL(string: topic.coverImg ?? "") {
            coverImage.downloadImage(from: url)
            coverImage.isHidden = false
            coverView.isHidden = false
        } else {
            coverImage.isHidden = true
            coverView.isHidden = true
            coverImage.image = UIImage(named: "")
        }
    }
    
    func convertNumber(str: String) -> String {
        let replaceString = str.replacingOccurrences(of: ",", with: "")
        let number = Int(replaceString) ?? 0
        
        switch number {
        case 0..<1000:
            return str
        case 1000...999999:
            return "\(number / 1000)k"
        case 1000000...9999999999:
            return "\(number / 1000000)m"
        default:
            return "\(number / 1000000000)b"
        }
    }
}
