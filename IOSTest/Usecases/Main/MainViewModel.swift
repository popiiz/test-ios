//
//  MainViewModel.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import Foundation
import UIKit

class MainViewModel: NSObject {
    
    var tags: [Tag] = []
    var topics: [Topic] = []
    var topicsTemp: [Topic] = []
    
    var reloadTopicCallback: (() -> ())?
    var reloadTagCallback: (() -> ())?
    
    func fetchTag() {
        let tagLoad = loadJson(fileName: "tags", type: TagLoad.self)
        if let tagLoad = tagLoad {
            self.tags = tagLoad.data
            reloadTagCallback?()
        }
    }
    
    func fetchTopic() {
        let topicLoad = loadJson(fileName: "topics", type: TopicLoad.self)
        if let topicLoad = topicLoad, let topicsList = topicLoad.data.topicList {
            self.topics = topicsList
            self.topicsTemp = topicsList
            reloadTopicCallback?()
        }
    }
    
    func loadMoreTopic() {
        topics = topics + topicsTemp
        reloadTopicCallback?()
    }
    
    func displayTopicCell(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TopicTableViewCell", for: indexPath) as? TopicTableViewCell {
            let topic = topics[indexPath.row]
            cell.setupInterface(topic: topic)
            return cell
        }
        return UITableViewCell()
    }
    
    func displayTagCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as? TagCollectionViewCell {
            cell.setupInterface(tag: tags[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
}

extension MainViewModel {
    func loadJson<T: Decodable>(fileName: String, type: T.Type) -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(type, from: data)
                print(jsonData)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

class CollectionViewFlowLayout : UICollectionViewFlowLayout {
    let partCellWidth = 135
    let partCellHeight = 130
    let partCellSpace = CGFloat(16)
    
    func configLayout() {
        
        self.scrollDirection = .horizontal
        self.itemSize = CGSize(width: partCellWidth, height: partCellHeight)
        self.minimumLineSpacing = partCellSpace
        self.sectionInset = UIEdgeInsets(top: partCellSpace, left: partCellSpace, bottom: partCellSpace, right: 0)
        
        if let collectionView = self.collectionView {
            collectionView.isPagingEnabled = true
        }
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.configLayout()
    }
}
