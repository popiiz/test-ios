//
//  Extensions+UIView.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import Foundation
import UIKit

extension UIView {
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: [ViewSide], withColor color: UIColor = .white, andThickness thickness: CGFloat) {

        side.forEach({
            let border = CALayer()
            border.backgroundColor = color.cgColor
            
            switch $0 {
            case .Left:
                border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height)
            case .Right:
                border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height)
            case .Top:
                border.frame = CGRect(x: 0.0, y: 0.0, width: frame.width, height: thickness)
            case .Bottom:
                border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
            }
            
            self.layer.addSublayer(border)
        })
    }
}
