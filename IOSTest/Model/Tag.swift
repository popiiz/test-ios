//
//  Tag.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import Foundation

struct TagLoad: Codable {
    var data: [Tag]
    var success: Bool
}

struct Tag: Codable {
    var name: String = ""
    var imageUrl: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case imageUrl = "image_url"
    }
}
