//
//  Topic.swift
//  IOSTest
//
//  Created by Saowalak Rungrat on 13/5/2564 BE.
//

import Foundation

struct TopicLoad: Codable {
    var data: TopicList
    var success: Bool
}

struct TopicList: Codable {
    var topicList: [Topic]?
    
    enum CodingKeys: String, CodingKey {
        case topicList = "topic_list"
    }
}

struct Topic: Codable {
    var title: String
    var author: String
    var viewCount: String
    var coverImg: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case author = "author"
        case viewCount = "view_count"
        case coverImg = "cover_img"
    }
}
